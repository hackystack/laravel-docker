# Laravel Docker Image
  Generic Docker image for Laravel Applications.


* This repo is based upon [laravel-docker](https://github.com/lorisleiva/laravel-docker) by [Loris Leiva](https://github.com/lorisleiva).


| Tags | PHP version | Features |
| - | - | - |
| 7.4 | 7.4 | ✅ Everything |
| latest | **7.4** | 🔗 Aliases the latest stable version of PHP available (even if that version does not support all features yet). |

#### Use within your GitLab's pipelines.
* [Run test suite and check codestyle](http://lorisleiva.com/using-gitlabs-pipeline-with-laravel/)
* [Build, test and deploy your Laravel application](http://lorisleiva.com/laravel-deployment-using-gitlab-pipelines/)@